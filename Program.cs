﻿using System;

namespace Parking
{
    class Program
    {
        static void Main(string[] args)
        {
            var parkingLot = new ParkingLot();
            var running = true;
            while (running)
            {
                var action = Console.ReadLine().Split(' ');

                switch (action[0])
                {
                    case "create_parking_lot":
                        Console.WriteLine("Created a parking lot with {0} slots", action[1]);
                        
                        if (!string.IsNullOrEmpty(action[1]))
                        {
                            var newParkingLot = new ParkingLot(Convert.ToInt32(action[1]));
                            parkingLot = newParkingLot;
                        }
                        break;
                    case "park":
                        if(parkingLot.IsFull())
                            Console.WriteLine("Sorry, parking lot is full");
                        else
                        {
                            var vehicle = new Vehicle() { RegistrationNo = action[1], Colour = action[2], VehicleType = (VehicleType)System.Enum.Parse(typeof(VehicleType),action[3]) };
                            parkingLot.Park(vehicle);
                        }
                        break;
                    case "leave":
                       if(parkingLot.Tickets.Count == 0)
                            Console.WriteLine("There is no vehicle");
                        else
                        {
                            parkingLot.Leave(Convert.ToInt32(action[1]));
                        }
                        break;
                    case "status":
                        parkingLot.GetStatus();
                        break;
                    case "type_of_vehicles":
                        parkingLot.CountByType(action[1]);
                        break;
                    case "registration_numbers_for_vehicles_with_ood_plate":
                        parkingLot.GetRegistrationNoByOodPlate();
                        break;
                    case "registration_numbers_for_vehicles_with_event_plate":
                        parkingLot.GetRegistrationNoByEvenPlate();
                        break;
                    case "registration_numbers_for_vehicles_with_colour":
                        parkingLot.GetRegistrationNoByColour(action[1]);
                        break;
                    case "slot_numbers_for_vehicles_with_colour":
                        parkingLot.GetSlotNoByColour(action[1]);
                        break;
                    case "slot_number_for_registration_number":
                        parkingLot.GetSlotNoByRegNo(action[1]);
                        break;
                    case "exit":
                        Console.WriteLine("Exiting . . .");
                        running = false;
                        break;
                    default:
                        Console.WriteLine("Command not found !");
                        break;
                }
            }
        }
    }
}
