﻿namespace Parking
{
    public class Vehicle
    {
        public string RegistrationNo { get; set; }
        public VehicleType VehicleType { get; set; }
        public string Colour { get; set; }
    }
}
