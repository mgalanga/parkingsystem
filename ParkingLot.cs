﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parking
{
    public class ParkingLot
    {
        private readonly int _totalParkingSpots;
        public List<Ticket> Tickets { get; set; } = new List<Ticket>();
        public ParkingLot()
        {
        }
        public ParkingLot(int totalParkingSpots)
        {
            _totalParkingSpots = totalParkingSpots;
        }

        public int GetTotalOccupiedSpots()
        {
            return Tickets.Count;
        }

        public bool IsFull()
        {
            if (Tickets.Count == _totalParkingSpots)
                return true;

            return false;
        }
        private int getMissingNo()
        {
            int n = this.Tickets.Count;
            int missingNo = (n + 1) * (n + 2) / 2;

            for (int i = 0; i < n; i++)
                missingNo -= this.Tickets[i].LotNumber;

            return missingNo;
        }
        public void Park(Vehicle vehicle)
        {
            var ticket = new Ticket() { EntryTime = DateTime.Now, Vehicle = vehicle, LotNumber = this.Tickets.Count == 0 ? 1 :  getMissingNo()  };
            Console.WriteLine("Allocated slot number {0}", this.Tickets.Count == 0 ? 1 : getMissingNo());
            this.Tickets.Add(ticket);
        }

        public void Leave(int lotNumber)
        {
            this.Tickets.RemoveAt(lotNumber - 1);
            Console.WriteLine("Slot number {0} is free", lotNumber);
        }
        public void GetStatus()
        {
            Console.WriteLine("Slot No. Registration No. Type Colour");
            foreach (var item in Tickets)
            {
                Console.WriteLine(item.LotNumber + " " + item.Vehicle.RegistrationNo + " " + item.Vehicle.VehicleType.ToString() + " " + item.Vehicle.Colour);
            }
        }

        public void CountByType(string vehicleType)
        {
            var total = this.Tickets.FindAll(v => v.Vehicle.VehicleType == (VehicleType)System.Enum.Parse(typeof(VehicleType), vehicleType)).Count;
            Console.WriteLine(total);
        }

        public void GetRegistrationNoByOodPlate()
        {
            List<string> listRegNo = new List<string>();
            foreach(var item in this.Tickets)
            {
                var plateNo = item.Vehicle.RegistrationNo.Split("-")[1];
                var lastNumber = plateNo.Substring(plateNo.Length - 1,1);
                if (Convert.ToInt32(lastNumber) % 2 != 0)
                    listRegNo.Add(item.Vehicle.RegistrationNo);
            }

            Console.WriteLine(string.Join(",",listRegNo));
        }

        public void GetRegistrationNoByEvenPlate()
        {
            List<string> listRegNo = new List<string>();
            foreach (var item in this.Tickets)
            {
                var plateNo = item.Vehicle.RegistrationNo.Split("-")[1];
                var lastNumber = plateNo.Substring(plateNo.Length - 1, 1);
                if (Convert.ToInt32(lastNumber) % 2 == 0)
                    listRegNo.Add(item.Vehicle.RegistrationNo);
            }

            Console.WriteLine(string.Join(",", listRegNo));
        }

        public void GetRegistrationNoByColour(string colour)
        {

            var listRegNo = this.Tickets.Where(v => v.Vehicle.Colour.Equals(colour)).Select(v => v.Vehicle.RegistrationNo);

            Console.WriteLine(string.Join(",", listRegNo));
        }
        public void GetSlotNoByColour(string colour)
        {
            var slotNo = this.Tickets.Where(v => v.Vehicle.Colour.Equals(colour)).Select(v => v.LotNumber).ToList();
            if (slotNo.Count > 0)
                Console.WriteLine(string.Join(",", slotNo));
            else
                Console.WriteLine("Not found");
        }

        public void GetSlotNoByRegNo(string regNo)
        {
            var slotNo = this.Tickets.Where(v => v.Vehicle.RegistrationNo.Equals(regNo)).Select(v => v.LotNumber).ToList();
            if (slotNo.Count > 0)
                Console.WriteLine(string.Join(",", slotNo));
            else
                Console.WriteLine("Not found");
        }


    }
}
