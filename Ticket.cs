﻿using System;

namespace Parking
{
    public class Ticket
    {
        private int _cost { get; set; }

        public int LotNumber { get; set; }
        public DateTime EntryTime { get; set; }
        public DateTime ExitTime { get; set; }
        public Vehicle Vehicle { get; set; }

        public int GetCost()
        {
            return _cost;
        }


    }
}
